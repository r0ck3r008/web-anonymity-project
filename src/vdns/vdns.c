#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<linux/if.h>
#include<linux/if_ether.h>
#include<linux/if_packet.h>
#include<errno.h>
#include<arpa/inet.h>

union ethframe
{
	struct
	{
		struct ethhdr header;
		unsigned char data[ETH_DATA_LEN];
	} field;
	unsigned char buffer[ETH_FRAME_LEN];
};

struct server_db
{
	char mainstr[40];
	unsigned char mac[ETH_ALEN];
	char name[20];
};

struct mac_db
{
	int priority;
	unsigned char mac[ETH_ALEN];
	char mainstr[25];
};

int count_vdns, count_server;

//prototypes
void *allocate(char *, int);
unsigned char *get_mac(char *);
int get_index(char *);
struct server_db* get_server_db(char *);
struct mac_db *get_mac_db(char *);

void *allocate(char *type, int size)
{
	void *ret=NULL;
	if(strcmp(type, "char")==0)
	{
		ret=malloc(size*sizeof(char));
		explicit_bzero(ret, size*sizeof(char));
	}
	else if(strcmp(type, "struct server_db")==0)
	{
		ret=malloc(size*sizeof(struct server_db));
		explicit_bzero(ret, size*sizeof(struct server_db));
	}
	else if(strcmp(type, "unsigned char")==0)
	{
		ret=malloc(size*sizeof(unsigned char));
		explicit_bzero(ret, size*sizeof(unsigned char));
	}
	else if(strcmp(type, "struct mac_db")==0)
	{
		ret=malloc(size*sizeof(struct mac_db));
		explicit_bzero(ret, size*sizeof(struct mac_db));
	}
	if(ret==NULL)
	{
		fprintf(stderr, "\n[-]Error in allocating %d bytes for %s type\n",
			size, type);
		_exit(-1);
	}
	return ret;
}

unsigned char *get_mac(char *ifname)
{
	unsigned char *mac=(unsigned char *)allocate("unsigned char", ETH_ALEN);

	struct ifreq req;
	explicit_bzero(&req, sizeof(req));

	strcpy(req.ifr_name, ifname);

	int s;
	if((s=socket(AF_INET, SOCK_STREAM, 0))<0)
	{
		fprintf(stderr, "\n[-]error in creating dummy sock for mac %s\n",
			strerror(errno));
		_exit(-1);
	}

	if(ioctl(s, SIOCGIFHWADDR, &req)==0)
	{
		memcpy(mac, req.ifr_addr.sa_data, ETH_ALEN);
	}

	printf("\n[!]Mac of %s is: ", ifname);
	for(int i=0;i<ETH_ALEN;i++)
	{
		printf("%02x", mac[i]);
		if(i!=ETH_ALEN-1)
		{
			printf(":");
		}
		else
		{
			printf("\n");
		}
	}

	close(s);
	return mac;
}

int get_index(char *ifname)
{
	int index;

	struct ifreq req;
	explicit_bzero(&req, sizeof(req));

	int s;
	if((s=socket(AF_INET, SOCK_STREAM, 0))<0)
	{
		fprintf(stderr, "\n[-]Error in creating dummy sock for index %s\n",
			strerror(errno));
		_exit(-1);
	}

	strcpy(req.ifr_name, ifname);

	if(ioctl(s, SIOCGIFINDEX, &req)==0)
	{
		index=req.ifr_ifindex;
	}

	printf("\n[!]The index of %s id : %d\n", ifname, index);

	close(s);
	return index;
}

struct server_db *get_server_db(char *fname)
{
	struct server_db *db=(struct server_db*)allocate("struct server_db", 10);
	FILE *serverdb;
	if((serverdb=fopen(fname, "r"))==NULL)
	{
		fprintf(stderr, "\n[-]Error in opening server_db file %s: %s\n", fname,
			strerror(errno));
		_exit(-1);
	}

	int count=0;

	//read file
	char u1;
	for(int j=0; !feof(serverdb);)
	{
		fscanf(serverdb, "%c", &u1);
		if(u1!='\n')
		{
			sprintf(&db[count].mainstr[j], "%c", u1);
			j++;
		}
		else
		{
			count++;
			j=0;
		}
	}

	count--;
	//break into mac
	char *tmp=(char*)allocate("char", 20);
	unsigned int u2;
	for(int i=0;i<count;i++)
	{
		strcpy(db[i].name, strtok(db[i].mainstr, "-"));
		strcpy(tmp, strtok(NULL, "-"));
		for(int j=0; j<ETH_ALEN;j++)
		{
			if(j!=0)
			{
				sscanf(strtok(NULL, ":"), "%02x", &u2);
			}
			else
			{
				sscanf(strtok(tmp, ":"), "%02x", &u2);
			}
			db[i].mac[j]=u2;
		}
	}

	for(int i=0;i<count;i++)
	{
		printf("\n[!]%s--> ", db[i].name);
		for(int j=0;j<ETH_ALEN;j++)
		{
			printf("%02x", db[i].mac[j]);
			if(j!=ETH_ALEN-1)
			{
				printf(":");
			}
			else
			{
				printf("\n");
			}
		}
	}

	count_server=count;
	return db;
}

struct mac_db *get_mac_db(char *fname)
{
	//open file
	FILE *macdb;
	if((macdb=fopen(fname, "r"))==NULL)
	{
		fprintf(stderr, "\n[-]Error in opening serverdb file %s: %s\n",
			fname, strerror(errno));
		_exit(-1);
	}

	//init struct
	struct mac_db *db=(struct mac_db*)allocate("struct mac_db", 10);
	int count=0;

	//read file and fill mainStrs
	char u1;
	for(int i=0, j=0;!feof(macdb);)
	{
		fscanf(macdb, "%c", &u1);
		if(u1!='\n')
		{
			sprintf(&db[count].mainstr[i], "%c", u1);
			i++;
		}
		else
		{
			count++;
			i=0;
		}
	}
	count--;

	//break to mac
	unsigned int u2;
	char *tmp=(char*)allocate("char", 20);
	for(int i=0;i<count;i++)
	{
		db[i].priority=(int)strtol(strtok(db[i].mainstr, "-"), NULL, 10);
		strcpy(tmp, strtok(NULL, "-"));
		for(int j=0;j<ETH_ALEN;j++)
		{
			if(j!=0)
			{
				sscanf(strtok(NULL, ":"), "%02x", &u2);
			}
			else
			{
				sscanf(strtok(tmp, ":"), "%02x", &u2);
			}
			db[i].mac[j]=u2;
		}
	}

	for(int i=0; i<count; i++)
	{
		printf("\n[!]For %d: ", db[i].priority);
		for(int j=0;j<ETH_ALEN;j++)
		{
			printf("%02x", db[i].mac[j]);
			if(j!=ETH_ALEN-1)
			{
				printf(":");
			}
			else
			{
				printf("\n");
			}
		}
	}
	count_vdns=count;
	return db;
}

int main(int argc, char *argv[])
{
	if(argc<4)
	{
		fprintf(stderr, "[!]Usage:\n./vdns [src_interface] [server_db_file] [mac_db_file]");
		_exit(-1);
	}

	//get src mac
	unsigned char *src_mac=(unsigned char*)allocate("unsigned char", ETH_ALEN);
	src_mac=get_mac(argv[1]);

	//get index
	int src_index=get_index(argv[1]);

	//get server_db
	struct server_db *serverdb=allocate("struct server_db", 10);
	serverdb=get_server_db(argv[2]);

	//get_mac_db
	struct mac_db *macdb=allocate("struct mac_db", 10);
	macdb=get_mac_db(argv[3]);

	//union
	union ethframe frame;
	explicit_bzero(&frame, sizeof(frame));

	//struct
	struct sockaddr_ll addrR, addrS;
	explicit_bzero(&addrR, sizeof(addrR));
	explicit_bzero(&addrS, sizeof(addrS));
	addrS.sll_family=PF_PACKET;
	addrS.sll_ifindex=src_index;
	addrS.sll_halen=ETH_ALEN;

	unsigned short proto=0x1234;

	//sock
	int sock;
	if((sock=socket(AF_PACKET, SOCK_RAW, htons(proto)))<0)
	{
		fprintf(stderr, "\n[-]Error in creating sock : %s\n", strerror(errno));
		_exit(-1);
	}

	//recvfrom
	socklen_t len=sizeof(addrR);
	int stamp;
	int pack_len;
	unsigned char *cmdr=(unsigned char*)allocate("unsigned char", 100);
	unsigned char *cmds=(unsigned char*)allocate("unsigned char", 100);
	unsigned char *data=(unsigned char*)allocate("unsigned char", 50);
	if(recvfrom(sock, cmdr, 100*sizeof(unsigned char), 0,
			(struct sockaddr*)&addrR, &len)<0)
	{
		fprintf(stderr, "\n[-]Error in recv from: %s\n", strerror(errno));
		_exit(-1);
	}
	else
	{
		//parse cmdr
		unsigned char *temp=(unsigned char *)allocate("unsigned char", 100);
		for(int i=ETH_HLEN-1, j=0;i<strlen(cmdr);i++,j++)
		{
			temp[j]=cmdr[i];
		}
		printf("\n[!]Received: %s\n", temp);

		//get stamp
		stamp=(int)strtol(strtok(temp, ":"), NULL, 10);
		printf("\n[!]Packet stamp= %d\n", stamp);

		//make data
		char *site=strtok(NULL, ":");// get name to site to reach
		sprintf(data, "%d:%s", stamp, strtok(NULL, ":"));
		pack_len=ETH_HLEN+strlen(data);
		printf("\n[!]Sending: %s\n", data);

		//make farme;
		frame.field.header.h_proto=htons(proto);
		memcpy(frame.field.data, data, strlen(data));
		memcpy(frame.field.header.h_source, src_mac, ETH_ALEN);

		//check if site is available
		int site_indx=-1;
		for(int i=0;i<count_server;i++)
		{
			if(strcmp(serverdb[i].name, site)==0)
			{
				site_indx=i;
				break;
			}
		}

		if(site_indx!=-1)
		{
			//send to site;
			memcpy(frame.field.header.h_dest, serverdb[site_indx].mac,
			       ETH_ALEN);
			if(sendto(sock, frame.buffer, pack_len, 0, (struct sockaddr*)&addrS,
				  sizeof(addrS))<0)
			{
				fprintf(stderr, "\n[-]Sendto failed: %s\n", strerror(errno));
				_exit(-1);
			}
			else
			{
				printf("\n[!]Sent to %s with packet len %d\n", site, pack_len);
			}
		}
		else
		{
			//get vdns priority number
			printf("[-]Selected site not in db.\n[!]Enter priority vdns number:\t");
			char *pristr=(char*)allocate("char", 5);
			int pri=(int)strtol(fgets(pristr, 5*sizeof(char), stdin), NULL, 10);
			printf("\n[!]Selected priority is: %d\n", pri);
			//send to next vdns
			for(int i=0;i<count_vdns;i++)
			{
				if(macdb[i].priority==pri)
				{
					//make frame
					memcpy(frame.field.header.h_dest, macdb[i].mac, ETH_ALEN);
					if(sendto(sock, frame.buffer, pack_len, 0,
						  (struct sockaddr*)&addrS, sizeof(addrS))<0)
					{
						fprintf(stderr, "[-]Error in sending to next vdns: %s\n",
							strerror(errno));
						_exit(-1);
					}
					else
					{
						printf("\n[!]Sent the packet to next vdns at mac: ");
						for(int j=0;j<ETH_ALEN;j++)
						{
							printf("%02x", macdb[i].mac[j]);
							if(j!=ETH_ALEN-1)
							{
								printf(":");
							}
							else
							{
								printf("\n");
							}
						}
					}
					break;
				}
			}
		}
	}
	return 0;
}
