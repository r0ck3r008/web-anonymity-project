#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<linux/if.h>
#include<linux/if_ether.h>
#include<linux/if_packet.h>
#include<sys/ioctl.h>
#include<errno.h>
#include<sys/socket.h>
#include<arpa/inet.h>

void *allocate(char *type, int size)
{
	void *ret=NULL;
	if(strcmp(type, "unsigned char")==0)
	{
		ret=malloc(size*sizeof(unsigned char));
		explicit_bzero(ret, size*sizeof(unsigned char));
	}
	if(ret==NULL)
	{
		fprintf(stderr, "[-]Error in allocating %d bytes fot %s type\n",
			size, type);
		_exit(-1);
	}
	return ret;
}

int main(int argc, char *argv[])
{

	unsigned short proto= 0x1234;

	//sock
	int sock;
	if((sock=socket(AF_PACKET, SOCK_RAW, htons(proto)))<0)
	{
		fprintf(stderr, "\n[-]Error in creating sock %s\n",
			strerror(errno));
		_exit(-1);
	}

	//struct
	struct sockaddr_ll addrR;
	explicit_bzero(&addrR, sizeof(addrR));
	socklen_t len=sizeof(addrR);

	unsigned char *cmdr=(unsigned char*)allocate("unsigned char", 100);
	unsigned char *tmp=(unsigned char*)allocate("unsigned char", 100);
	int stamp;
	//recv
	if(recvfrom(sock, cmdr, 100*sizeof(unsigned char), 0,
			(struct sockaddr*)&addrR, &len)<0)
	{
		fprintf(stderr, "\n[-]Recv from failed %s\n", strerror(errno));
		_exit(-1);
	}
	else
	{
		for(int i=ETH_HLEN-1, j=0;i<strlen(cmdr); i++, j++)
		{
			tmp[j]=cmdr[i];
		}
		printf("\n[!]Received: %s\n", tmp);
		stamp=(int)strtol(strtok(tmp, ":"), NULL, 10);
		printf("\n[!]Stamp is : %d\n", stamp);
	}

	printf("\n[!]Done\n");
}
