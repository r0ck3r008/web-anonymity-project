#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<linux/if.h>
#include<linux/if_ether.h>
#include<linux/if_packet.h>
#include<sys/ioctl.h>
#include<errno.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sodium.h>

union ethframe
{
	struct
	{
		struct ethhdr header;
		unsigned char data[ETH_DATA_LEN] ;
	} field;
	unsigned char buffer[ETH_FRAME_LEN];
};

void *allocate(char *type, int size)
{
	void *ret=NULL;
	if(strcmp(type, "unsigned char")==0)
	{
		ret=malloc(size*sizeof(unsigned char));
		explicit_bzero(ret, size*sizeof(unsigned char));
	}
	if(ret==NULL)
	{
		fprintf(stderr, "[-]Error in allocating %d bytes for %s type\n",
			size, type);
		_exit(-1);
	}
	return ret;
}

unsigned char *get_mac(char *ifname)
{
	unsigned char *mac=(unsigned char *)allocate("unsigned char", ETH_ALEN);
	explicit_bzero(mac, ETH_ALEN);

	struct ifreq req;
	explicit_bzero(&req, sizeof(req));

	strcpy(req.ifr_name, ifname);

	int s;
	if((s=socket(AF_INET, SOCK_STREAM, 0))<0)
	{
		fprintf(stderr, "[-]Error in creating dummy mac sock %s\n",
			strerror(errno));
		_exit(-1);
	}

	if(ioctl(s, SIOCGIFHWADDR, &req)==0)
	{
		memcpy(mac, req.ifr_addr.sa_data, ETH_ALEN);
	}

	printf("\n[!]Mac of %s: ", ifname);
	for(int i=0;i<ETH_ALEN;i++)
	{
		printf("%02x", mac[i]);
		if(i!=ETH_ALEN-1)
		{
			printf(":");
		}
	}
	close(s);
	return mac;
}

int get_index(char *ifname)
{
	int index;

	struct ifreq req;
	explicit_bzero(&req, sizeof(req));

	int s;
	if((s=socket(AF_INET, SOCK_STREAM, 0))<0)
	{
		fprintf(stderr, "[-]Error in creating dummy sock: %s\n",
			strerror(errno));
		_exit(-1);
	}

	strcpy(req.ifr_name, ifname);

	if(ioctl(s, SIOCGIFINDEX, &req)==0)
	{
		index=req.ifr_ifindex;
	}

	printf("\n[!]Index of %s is %d\n", ifname, index);

	close(s);
	return index;
}
int main(int argc, char *argv[])
{
	if(argc<3)
	{
		fprintf(stderr, "\n[!]Usage:\n./client [src_mac] [vdns_mac]\n");
		_exit(-1);
	}

	if(sodium_init()!=0)
	{
		fprintf(stderr, "\n[-]Error in initialising libsodium\n");
		_exit(-1);
	}

	unsigned char *src_mac=allocate("unsigned char", ETH_ALEN);
	unsigned char *dns_mac=allocate("unsigned char", ETH_ALEN);
	unsigned int u;
	src_mac=get_mac(argv[1]);

	printf("\n[!]Vdns mac is: ");
	for(int i=0;i<ETH_ALEN;i++)
	{
		if(i!=0)
		{
			sscanf(strtok(NULL, ":"), "%02x", &u);
		}
		else
		{
			sscanf(strtok(argv[2], ":"), "%02x", &u);
		}
		dns_mac[i]=u;
		printf("%02x", dns_mac[i]);
		if(i!=ETH_ALEN-1)
		{
			printf(":");
		}
		else
		{
			printf("\n");
		}
	}

	//initialise
	unsigned short proto=0x1234;
	unsigned char *data=(unsigned char*)allocate("unsigned char", 30);
	unsigned char *datatmp=(unsigned char*)allocate("unsigned char", 20);
	printf("\n[>] ");
	sprintf(data, "%d:%s", randombytes_uniform(1000000),
		fgets(datatmp, 20*sizeof(unsigned char), stdin));
	printf("i\n[!]Sending: %s\n", data);
	unsigned short frame_len=strlen(data)+ETH_HLEN;
	free(datatmp);

	//union
	union ethframe frame;
	explicit_bzero(&frame, sizeof(frame));
	memcpy(frame.field.header.h_dest, dns_mac, ETH_ALEN);
	memcpy(frame.field.header.h_source, src_mac, ETH_ALEN);
	frame.field.header.h_proto=htons(proto);
	memcpy(frame.field.data, data, 30*sizeof(unsigned char));

	//struct
	struct sockaddr_ll addrS;
	explicit_bzero(&addrS, sizeof(addrS));
	addrS.sll_family=PF_PACKET;
	addrS.sll_ifindex=get_index(argv[1]);
	addrS.sll_halen=ETH_ALEN;

	//sock
	int sock;
	if((sock=socket(AF_PACKET, SOCK_RAW, htons(proto)))<0)
	{
		fprintf(stderr, "\n[-]Error in creating sock %s\n",
			strerror(errno));
		_exit(-1);
	}

	//send
	for(int i=0;i<10;i++)
	{
		if(sendto(sock, frame.buffer, frame_len, 0,
				(struct sockaddr*)&addrS, sizeof(addrS))<0)
		{
			fprintf(stderr, "\n[-]Sendto failed %s\n",
				strerror(errno));
			_exit(-1);
		}
		else
		{
			printf("\n[!]Packet sent with length %d\n", frame_len);
		}
		sleep(1);
	}

	printf("\n[!]Done\n");

}
