# Web Anonymity Framework

#### Introduction
This project is implemented as solely a layer 2 networking stack. It __does not require IP for communication__. This is a proof of concept that scalable networking is possible with just the use of hardware MAC addresses, thus preserving anonymity as a side effect.

#### Highlights
* Implements IP less networking framework
* Uses a series of interconnected Virtual DNS resolvers to relay data back and fourth
* The Virtual DNS servers also resolve legacy common names of servers to mantain current browser compatibility
* Protects location based tracking by eleminating IP addresses and thus all associated location information

#### Architecture
![Architecture](./doc/architecture.jpg)

#### Dependencies
* GNU Make
* GNU GCC
* Libsodium

#### Compilation
```bash
cd src
#compile
make
#clean
make clean
```

#### Execution
Binaries are available in src/bin after compilation succeedes. All the necessary help is inbuild to code.
